import {weatherAPI} from "../../api/api";

const SET_CITY = 'SET_CITY';
const SET_WEATHER = 'SET_WEATHER';
const SET_ERROR = 'SET_ERROR';

let initialState = {
    location: '',
    responseLocation: '',
    country: '',
    temp_c: '',
    wind: '',
    pressure: '',
    humidity: '',
    condition: '',
    icon: 'https:' + '',
    background: '',
    threeDayForecast: [],
    error: ''
}

const weatherReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CITY:
            return {
                ...state,
                location: action.payload,
            };
        case SET_WEATHER:
            return {
                ...state,
                responseLocation: action.payload.data.location.name,
                country: action.payload.data.location.country,
                temp_c: action.payload.data.current.temp_c.toFixed(),
                wind: action.payload.data.current.wind_kph,
                pressure: action.payload.data.current.pressure_mb,
                humidity: action.payload.data.current.humidity,
                condition: action.payload.data.current.condition.text.toLowerCase(),
                icon: action.payload.data.current.condition.icon,
                background: action.payload.data.current.is_day,
                threeDayForecast: action.payload.data.forecast.forecastday.map((day) => {
                    return {
                        date: day.date,
                        temp_c: day.day.avgtemp_c,
                        icon: 'https:' + String(day.day.condition.icon),
                    }
                }),
            };
        case SET_ERROR:
            return {
                ...state,
                error: action.payload,
            }
        default:
            return state;
    }
}
//Action Creators:
export const setCity = (city) => ({ type: SET_CITY, payload: city});
export const setWeather = (response) => ({type: SET_WEATHER, payload: response})
export const setError = (error) => ({type: SET_ERROR, payload: error})

// Thunks:
export const getWeather = (city, language) => {
    return (dispatch) => {
        weatherAPI.getCurrentWeather(city, language)
            .then(response => {
                if (response.status === 200) {
                    dispatch(setError(null));
                }
                dispatch(setWeather(response));
                dispatch(setBackground(response.data.current.is_day));
        })
            .catch(error => {
                if (error.response.data.error.code === 1006) {
                        dispatch(setError(error.response.data.error.code));
                    }
            })
    }
}
const setBackground = (props) => {
    let containerElement = document.querySelector('html');
    if (props) {
        containerElement.style.backgroundColor = "#42C2FF";
    } else {
        containerElement.style.backgroundColor = "#712B75";
    }
}
export default weatherReducer;

import React from 'react';
import './index.scss';
import App from './App';
import {Provider} from "react-redux";
import store from "./store/store";
import ReactDOM from 'react-dom/client';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>
);

import React, {useEffect, useState} from "react";
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {IntlProvider} from "react-intl";
import {messages} from "./misc/messages";
import {LOCALES} from "./misc/locales";
import {connect} from "react-redux";
import {getWeather} from "./store/weatherReducer/weatherReducer";
import store from "./store/store";
import ForecastPage from "./components/ForecastPage";
import WeatherData from "./components/WeatherData";

const App = (props) => {
    let [currentLocale, setCurrentLocale] = useState(getInitialLocal);
    function getInitialLocal() {
        const savedLocale = localStorage.getItem("locale");
        return savedLocale || LOCALES.RUSSIAN;
    }
    let handleChange = (lang) => {
        setCurrentLocale(lang);
        localStorage.setItem("locale", lang);
        if (props.location || props.responseLocation) {
            props.getWeather(props.location || props.responseLocation, lang);
        }
    }
    useEffect(() => {
        store.dispatch(getWeather('Москва', currentLocale));
    }, []);
    return (
        <BrowserRouter>
            <IntlProvider messages={messages[currentLocale]}  locale={currentLocale} defaultLocale={LOCALES.RUSSIAN}>
            <div className='app-wrapper'>
                <Routes>
                    <Route  path='/' element={<WeatherData handleChange={handleChange} />}/>
                    <Route  path='/forecast' element={<ForecastPage />} />
                </Routes>
            </div>
                </IntlProvider>
        </BrowserRouter>
    );
}

const mapStateToProps = (state) => ({
    location: state.weatherData.location,
    responseLocation: state.weatherData.responseLocation,
})

export default connect(mapStateToProps, {getWeather})(App) ;


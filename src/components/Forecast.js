import React from "react";
import moment from "moment";
import styles from "../assets/styles/WeatherData.module.scss";
import 'moment/locale/ru';

const Forecast = (props) => {
    if (localStorage.getItem("locale") === 'ru') {
        moment.locale('ru');
    } else {
        moment.locale('en');
    }
    return (
        <div className={styles.forecastItem}>
            <h3>
                {moment(props.date, 'YYYY-MM-DD').calendar({
                    sameDay: '',
                    nextDay: ``,
                    nextWeek: 'dddd',
                }).split(' ').join('').replace(/,в0:00|at12:00AM/, "")}
            </h3>
            <img src={props.icon} alt=''/>
            <p>
                {props.temp.toFixed()}°
            </p>
        </div>
    )
}
export default Forecast;
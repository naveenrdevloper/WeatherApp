import React from "react";
import styles from "../assets/styles/WeatherData.module.scss";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import Forecast from "./Forecast";
import {FormattedMessage} from "react-intl";

const ForecastPage = (props) => {
    let forecastElements = props.threeDayForecast.map( (day, index) => <Forecast key={index} date={day.date} temp={day.temp_c} icon={day.icon } />)
    return (
        <div className={styles.wrapper}>
            <NavLink to='/' className={styles.forecastButton}>
                <p><FormattedMessage id='back'/></p>
            </NavLink>
            <h1>{props.location}</h1>
            <h2>{props.country}</h2>
            <div className={styles.forecast}>
                {forecastElements}
            </div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    location: state.weatherData.responseLocation,
    country: state.weatherData.country,
    threeDayForecast: state.weatherData.threeDayForecast,
})
export default connect(mapStateToProps, null)(ForecastPage);

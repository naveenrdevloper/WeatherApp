import React from "react";
import styles from '../assets/styles/Search.module.scss'
import icon from '../assets/images/icon.png'
import {connect} from "react-redux";
import {getWeather, setCity} from "../store/weatherReducer/weatherReducer";
import LanguageButtons from "./LanguageButtons";

const Search = (props) => {
    const language = localStorage.getItem("locale");
    return (
        <div className={styles.wrapper}>
            <div className={styles.search}>
                <input placeholder='Search' type='text' onChange={(e) => props.onChange(e)}  value={props.location}
                       onKeyPress={event =>{if(event.key === 'Enter') {props.onWeather(props.location,language)}}} />
                <img src={icon} alt='' className={styles.icon} onClick={() => {props.onWeather(props.location,language)}} />
            </div>
            <LanguageButtons getWeather={props.getWeather} responseLocation={props.responseLocation} handleChange={props.handleChange} />
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    onChange: (e) => {
        dispatch(setCity(e.target.value));
    },
    onWeather: (location, language) => {
        dispatch(getWeather(location,language));
    }
});

export default connect(null, mapDispatchToProps)(Search);
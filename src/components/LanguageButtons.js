import React from "react";
import styles from '../assets/styles/LanguageButtons.module.scss'

const LanguageButtons = (props) => {
    return (
        <div className={styles.languages}>
            <button className={styles.russian} type='button' onClick={ () => props.handleChange('ru') }>RU</button>
            <button className={styles.english} type='button' onClick={ () => props.handleChange('en') }>EN</button>
        </div>
    )
}

export default LanguageButtons;
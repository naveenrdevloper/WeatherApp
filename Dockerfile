FROM node:16.14.2
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package-lock.json ./
COPY package.json ./
RUN npm install
RUN npm install react-scripts@5.0.1 -g
COPY . ./
CMD ["npm", "start"]

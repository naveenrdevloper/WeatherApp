## Weather App is a small SPA
This is my first SPA using a combination of React and Redux.
Features:

 - City search.
 - Page with a three-day weather forecast.
 - The ability to switch the languages between English and Russian.
 
In my work I used moment.js.


### How to use
  
1. You need to register at [weatherapi.com](https://www.weatherapi.com/docs/#) and get an API key.
2. Clone this repository `git clone https://gitlab.com/repeckaya/WeatherApp.git`
3. Go to the cloned directory (e.g. `cd WeatherApp`).
4. Add the API key to the `.env` file
5. Run `npm install`
6. Run `npm start`

The command will start a local live server. Open (http://localhost:3000/) in your browser, if the tab doesn't open automatically.

#### Contact me
[Telegram](https://t.me/repetskaya_oksana)